<h1>Name<\h1>

a01-website

<h1>Description

This is the first assignment for the course 1DV525 on Linneaus Univeristy.
The goal of this assignment is to create a website using HTML, CSS and JavaScript.
Link to the coursepress site where the assignment is described: https://coursepress.lnu.se/courses/introduction-to-web-programming/10-part-1/10-assignment-01
(Note you may have to supply credentials to access the coursepress site.)

<h1>Badges

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

<h1>Visuals

None.

<h1>Installation

N/A

Usage
(Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.)

Support
Contact information for support: ej222up@student.lnu.se

Roadmap
-

Authors and acknowledgment
Elias Johansson

License
None

Project status
Ongoing.
